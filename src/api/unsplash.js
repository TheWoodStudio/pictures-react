import axios from 'axios';

let url = `https://api.unsplash.com`;
const key = '6ffa801e16b246e70b22ec3285eeea32ef27cb0645c4f86068df1f3f63ca96fd';

export default axios.create({
    baseURL: url,
    headers: {
        Authorization: `Client-ID ${key}`
    }
});