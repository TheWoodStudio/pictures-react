import React, { Component } from 'react';

class SearchBar extends Component {
    state = { 
        inputValue: ''
     }

    onInputChange = (e) => {
        this.setState({
            inputValue: e.target.value
        });
    }

    onFormSubmit = (e) => {
        e.preventDefault();
        this.props.onFormSubmit(this.state.inputValue);
    }

    render() { 
        return ( 
            <form onSubmit={this.onFormSubmit}>
                <div className="ui icon input fluid">
                    <input 
                        type="text" 
                        placeholder="Search for images..." 
                        value={this.state.inputValue} 
                        onChange={this.onInputChange} 
                    />
                    <i className="search icon"></i>
                </div>
            </form>
         );
    }
}
 
export default SearchBar;