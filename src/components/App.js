import React, { Component } from 'react';
import unsplash from '../api/unsplash';
import SearchBar from './SearchBar';
import ImageList from './ImageList';

class App extends Component {
  state = {
    images: []
  }

  onFormSubmit = async (inputValue) => {

    const response = await unsplash.get('/search/photos', {
      params: {
        query: inputValue
      }, 
    });

    this.setState({
      images: response.data.results
    });
  }

  render() {
    return (
      <div className="App ui container">
        <SearchBar 
          onFormSubmit={this.onFormSubmit}
        />
        <ImageList 
          images={this.state.images}
        />
      </div>
    );
  }
}

export default App;
